﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataBase.Domain.Startup))]
namespace DataBase.Domain
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
