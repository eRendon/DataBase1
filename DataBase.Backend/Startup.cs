﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataBase.Backend.Startup))]
namespace DataBase.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
