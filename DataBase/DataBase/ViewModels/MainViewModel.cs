﻿namespace DataBase.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Domain.Models;
    using Models;

    public class MainViewModel : BaseViewModel
    {

        #region Properties

        public TokenResponse Token
        {
            get;
            set;
        }


        #endregion

        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }


        public RegisterViewModels Register
        {
            get;
            set;
        }


        public ChangePasswordViewModel ChangePassword
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Methods
        #endregion
    }
}