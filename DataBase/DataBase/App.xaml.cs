﻿namespace DataBase
{
    using Xamarin.Forms;
    using Views;
    using ViewModels;
    using Helpers;
    using Models;
    using Services;
    using System;
    using System.Threading.Tasks;

    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator
        {
            get;
            internal set;
        }

        #endregion

        #region Constructors

        #endregion

        #region Methods

        public static Action HideLoginView
        {
            get
            {
                return new Action(() => Application.Current.MainPage =
                                  new NavigationPage(new StartPage()));
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
        #endregion
    }
}